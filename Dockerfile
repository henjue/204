FROM golang:1.5
MAINTAINER henjue@gmail.com
ADD . .
EXPOSE 9090
CMD ["go","run","web.go"]
